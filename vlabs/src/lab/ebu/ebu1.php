<?php
session_start(); 
?>
<html lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1">
	<link rel="stylesheet" href="css/jquery.stopwatch.css" />
<script type="text/javascript" src="js/jquery.js"></script>
    <!--[if IE]><script language="javascript" type="text/javascript" src="../excanvas.min.js"></script><![endif]-->
    <script language="javascript" type="text/javascript" src="jquery.js"></script>
    <script language="javascript" type="text/javascript" src="jquery.flot.js"></script>

	<style type="text/css" media="screen">
body
{
 font-family: "Helvetica Neue", "Lucida Grande", Helvetica, Arial, Verdana, sans-serif;
        font-size: 14px;
        margin-top: .5em; color: #666;

}
#container
{
width: 90%;
margin: 10px auto;
background-color: #fff;
color: #333;
border: 10px solid gray;
line-height: 130%;
}

#top
{
padding: .5em;
background-color: #FFFFFF;
border-bottom: 0px solid gray;
}

#top h1
{
padding: 0;
margin: 0;
}

#leftnav
{
float: left;
width: 160px;
margin: 0;
padding: 1em;
}

#rightnav
{
float: right;
width: 20%;
margin: 0;
padding: 1em;
}

#content
{
width: 800;
margin-left: 0px;
border-left: 0px solid gray;
margin-right: 0px;
border-right: 0px solid gray;
padding: 1em;
#max-width: 36em;
}


#content2
{
width: 800;
margin-left: 0px;
border-left: 0px solid gray;
margin-right: 0px;
border-right: 0px solid gray;
border-top: 0px solid gray;
padding: 1em;
#max-width: 36em;
}

#footer
{
clear: both;
margin: 0;
padding: .5em;
color: #333;
background-color: #ddd;
border-top: 0px solid gray;
}

#leftnav p, #rightnav p { margin: 0 0 1em 0; }
#content h2 { margin: 0 0 .5em 0; }





</style> 
<script type="text/javascript"
  src="dygraph-combined.js"></script>


</head>
<body>


<div id="container">
<div id="top">
<a href=index.html><img border=0 src=vlabs.jpg></a><br>
</div>


<div id="content">

<? 
	
	
	$pres = $_POST["pressure"];

	$message = "x$".$pres;
	

	// where is the socket server?
	$host="10.102.26.50";
	$port = 14001;
	// create socket
	$socket = socket_create(AF_INET, SOCK_STREAM, 0) or die("Could not create socket\n");

	// connect to server
	$socket1 = socket_connect($socket, $host, $port) or die("Could not connect to server\n");

	//send string to server
	socket_write($socket, $message, strlen($message)) or die("Could not send data to server\n");
	
	$result = socket_read($socket, 3072) or die("Could not read server response\n");
       
 

	// get server response

	$res = explode("#",$result);
       
 	$size = sizeof($res);
	  //echo $size;
	 //print_r ($res);

	
	  $val_300  = $res[$size - 2];
	// get server response








	
?>
<script language="javascript"> 
	function showValues(){
		var a=new Array;
	<?
		for($i=0;$i<count($res); $i++){
			echo "a[$i]='".$res[$i]."';\n";
		}			
	 ?>
		for(i=0;i<a.length;i++)
			alert(a[i]);
	}






(function($) {
	jQuery.fn.stopwatch = function() {
		var clock = $(this);
		var timer = 0;
		
		clock.addClass('stopwatch');
		
		// This is bit messy, but IE is a crybaby and must be coddled. 
		clock.html('<div class="display"><span class="hr">00</span>:<span class="min">00</span>:<span class="sec">00</span></div>');
		clock.append('<input type="button" class="start" value="Start" />');
		clock.append('<input type="button" class="stop" value="Stop" />');
		clock.append('<input type="button" class="reset" value="Reset" />');
		
		// We have to do some searching, so we'll do it here, so we only have to do it once.
		var h = clock.find('.hr');
		var m = clock.find('.min');
		var s = clock.find('.sec');
		var start = clock.find('.start');
		var stop = clock.find('.stop');
		var reset = clock.find('.reset')
		var z = 1;
		var a=new Array;
	<?
		for($i=0;$i<count($res); $i++){
			echo "a[$i]='".$res[$i]."';\n";
		}			
	 ?>

			
	
		
		stop.hide();

		start.bind('click', function() {
			timer = setInterval(do_time, 1000);
			stop.show();
			start.hide();
		 document.getElementById('b').style.display = 'inline';
		document.getElementById('be').style.display = 'none';
   
		});
		
		stop.bind('click', function() {
			clearInterval(timer);
			timer = 0;
			start.show();
			stop.hide();
		});
		
		reset.bind('click', function() {
			clearInterval(timer);
			timer = 0;
			h.html("00");
			m.html("00");
			s.html("00");
			stop.hide();
			start.show();
			document.getElementById('be').style.display = 'inline';
			document.getElementById('b').style.display = 'none';
		});
		
		function do_time() {
			// parseInt() doesn't work here...
			hour = parseFloat(h.text());
			minute = parseFloat(m.text());
			second = parseFloat(s.text());
			
			second = second + 6;
			
			if(second > 59) {
				second = 0;
				minute = minute + 1;
			}
			if(minute > 59) {
				minute = 0;
				hour = hour + 1;
			}
			
			h.html("0".substring(hour >= 10) + hour);
			m.html("0".substring(minute >= 10) + minute);
			s.html("0".substring(second >= 10) + second);

			document.time.timer.value= "".substring(second >= 10) + second ;
							

					
				
			
			document.time.tmpr.value  = a[z];
			document.time.drate.value = Math.floor((9*a[z] - 380)/4);
			
	

			if(a[z+1] != "")
			{
			z = z + 1;
			}
			
  g = new Dygraph(

    // containing div
    document.getElementById("graphdiv"),

    // CSV or path to a CSV file.
    "Date,Temperature\n" +
    "2008-05-07,75\n" +
    "2008-05-08,70\n" +
    "2008-05-09,80\n"

  );

		}
	}
})(jQuery);









 </script>


<script type="text/javascript">

 </script>
<script type="text/javascript">
		$(function() {
			$('#clock1').stopwatch();
		});
	</script>



</div>
<div id="content">


		<div id="clock1"></div>
		<br /><br />
    
    <form name="time" method="post" action="ebu2.php">

<h3>Saturation Temperature  <INPUT style="width:150px;height:50px;background-color:#D0F18F;color:#53760D;font:24px/30px cursive;border:solid 1px #6DB72C;background-color:#D0F18F;" TYPE="text" VALUE="" NAME="tmpr"><SUP>o</sup>C<br>  Drop rate  <INPUT style="width:150px;height:50px;background-color:#D0F18F;color:#53760D;font:24px/30px cursive;border:solid 1px #6DB72C;background-color:#D0F18F;" TYPE="text" VALUE="" NAME="drate">Drops per minute</h3><br>

<h3>Enter pressure  <INPUT style="width:150px;height:50px;background-color:#D0F18F;color:#53760D;font:24px/30px cursive;border:solid 1px #6DB72C;background-color:#D0F18F;" TYPE="text" VALUE="" NAME="pressure"> mmHg </h3>
<input name=val_300 type="hidden" value="<? echo $val_300;?>"><br>
<input name=oldpres type="hidden" value="<? echo $pres;?>"><br>
<input name=timer type="hidden" value=""><br>
<input type="image" src="next.jpg" alt="Submit button">
</form>

<div id="graphdiv"
  style="width:800px; height:300px;"></div>
<script type="text/javascript">
  
</script>



<script id="source" language="javascript" type="text/javascript">

</script>



<div id="footer">
Cellulose lab IIT Bombay
</div>
</div>
</body>
</html>


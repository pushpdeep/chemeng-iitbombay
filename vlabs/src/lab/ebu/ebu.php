<HTML>
<HEAD>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
<TITLE>Virtual labs project</TITLE>
<STYLE>
body, input{
	font-family: Calibri, Arial;
}
#accordion {
	list-style: none;
	padding: 0 0 0 0;
	width: 600px;
}
#accordion li{
	display: block;
	background-color: #FF9927;
	font-weight: bold;
	margin: 1px;
	cursor: pointer;
	padding: 5 5 5 7px;
	list-style: circle;
	-moz-border-radius: 10px;
	-webkit-border-radius: 10px;
	border-radius: 10px;
}
#accordion ul {
	list-style: none;
	padding: 0 0 0 0;
	display: none;
}
#accordion ul li{
	font-weight: normal;
	cursor: auto;
	background-color: #fff;
	padding: 0 0 0 7px;
}
#accordion a {
	text-decoration: none;
}
#accordion a:hover {
	text-decoration: underline;
}

</STYLE>
</HEAD>
<BODY>
<center>
<H2>Ebulliometric determination of vapour pressure</H2>
<br>
<h4>
In the following exercises you will learn about ebulliometric determination of vapour pressure</h4>
<ul id="accordion">
	<li>On what factors does the vapour pressure depend?</li>
	<ul>
		<li>Molecular structure and bonding - If a liquid or solid is made up of molecules that are bonded together strongly, the vapor pressure will be relatively low. If the molecules are connected with weaker bonds, the resulting vapor pressure will be relatively higher.
Temperature - This experiment is all about correlation of vapour pressure and temperature (of water).
Mole fraction in case of mixture (for ideal mixture) - affective vapor pressure measured from a mixture of two liquid is equal to that of a mole fraction weighed pressure of its components. In non ideal liquid mixtures deviations can occur from this value. This is also called as rault's law. </li>

	</ul>
	<li>Whats the simplest way of predicting the vapour pressure theoretically?</li>
	<ul>
		<li> Iteratively you can reach at Vapour pressure from a initial guess with the terminating condition of equal fugacities in both phases. Fugacities can be calculated from equation of states like Peng Robinson or SRK.</li>
	</ul>
	<li>Does surface area of liquid in contact with vapor affect vapour pressure?</li>
	<ul>
		<li> No, since molecules in air and liquid are in dynamic equilibrium and increased surface area means molecules going from liquid to vapour increase as well as molecules coming into the liquid from air also increase. There is no change in overall equilibrium value of pressure. </li>
	</ul>


	<li>What is Vapour Pressure of Water at absolute Zero? (Hint: Kinetic Theory of Gases)</li>
	<ul>
		<li>Zero</li>
	</ul>


	
</ul>
<br>
<br>
<a href="ebu0.php?mode=start"><img border=0 src="next.jpg"></a>
</center>
</BODY>
<SCRIPT>
$("#accordion > li").click(function(){

	if(false == $(this).next().is(':visible')) {
		$('#accordion > ul').slideUp(300);
	}
	$(this).next().slideToggle(300);
});

$('#accordion > ul:eq(0)').show();

</SCRIPT>
</HTML>
